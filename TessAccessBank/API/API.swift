//
//  API.swift
//  TessAccessBank
//
//  Created by AccessBank on 7/29/20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import Foundation


// MARK: - Base Method

public struct API {
    
    enum ResponseType {
        case object
        case array
    }
    

    func RequestAPI<D>(model: D.Type, responseType: ResponseType = .object, urlType: String, param: [String: Any]? = nil, method: HTTPMethod = .get, header: HTTPHeaders? = nil, completion: @escaping(_ objectArray: ResultArray<D>?, _ objectData: ResulstObject<D>?) -> ()) {
        
        Request(shortUrl: urlType, params: param, method: method, headers: header) { (data, error) in
            
            if let err = error {
                print(err.localizedDescription)
                return
            }
            
            switch responseType {
                
            case .object:
                if let responseObject = self.JSONParseDecodable(model: ResulstObject<D>.self, dataSnapshot: data) {
                    completion(nil, responseObject)
                }
            case .array:
                if let responseArray = self.JSONParseDecodable(model: ResultArray<D>.self, dataSnapshot: data) {
                    completion(responseArray, nil)
                }
            }
        }
        
    }
    
       
    func Request(shortUrl: String, params: [String: Any]? = nil, method: HTTPMethod, headers: HTTPHeaders? = nil, completion: @escaping(_ data: Data?, _ error: Error?) -> ()) {
        
        guard let rUrl = URL(string: "\(baseURL)\(shortUrl)") else { return }
        
                
        request(rUrl, method: method, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            if let value = response.value {
                print("------(\(shortUrl))-------\n", value)
            }
            
            switch response.result {
                case .success:
                    completion(response.data, nil)
                case .failure:
                    completion(nil, response.error)
                
            }
        }
    }
    
    
    
    func JSONParseDecodable<T>(model: T.Type, dataSnapshot: Data?) -> T? {
                             
        if let data = dataSnapshot {
          let resultModel = self.decode(model: model.self, data: data) as? T
          return resultModel
        }
                
        return nil
    }
    

    func decode<T>(model: T.Type, data: Data) -> Decodable? {
        
        if let decodableType = model as? Decodable.Type,
            let myStruct = try? decodableType.init(jsonData: data) {
            return myStruct
        }
        
        return nil
    }


    
    func DateFormatForString(dateStr: String?, dateFormat: String, format: String) -> String? {
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        if let dateString = dateStr {
            if let date = dateFormatter.date(from: dateString) {
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = format
                return dateFormat.string(from: date)
            }
        }
        
        return nil
    }

}

extension Decodable {
    init(jsonData: Data) throws {
        self = try JSONDecoder().decode(Self.self, from: jsonData)
    }
}
