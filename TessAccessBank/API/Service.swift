//
//  Service.swift
//  TessAccessBank
//
//  Created by Habiboff on 12.07.20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import UIKit


let Services = Service.init()
let baseURL = "https://dev.json.az/api/"

struct Service {
    
    let api = API.init()
    
    let customer = "customer"
    typealias Customer = (([CustomerModel]) -> ())
    
    
    func fetchAllCustomer(completion: @escaping([CustomerModel]?) -> ()) {
        
        api.Request(shortUrl: customer, params: nil, method: .get, headers: nil) { (snapshot, error) in
                        
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            let customer = self.api.JSONParseDecodable(model: [CustomerModel].self, dataSnapshot: snapshot)
            completion(customer)
            
            
            
            
            //let data = self.JSONParseDecodable(model: MainModel<CustomerModel>.self, dataSnapshot: snapshot)
            
            
        }
        
    }
    
    
    func fetchUserById(id: Int, completion: @escaping(CustomerModel?) -> ()) {
        
        api.Request(shortUrl: "\(customer)/\(id)", params: nil, method: .get, headers: nil) { (snapshot, error) in

            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            let customer = self.api.JSONParseDecodable(model: CustomerModel.self, dataSnapshot: snapshot)
            completion(customer)
        }
    }
    
    
    
    func addUserPost(completion: @escaping([CustomerModel]?) -> ()) {
        
        let param = ["Name": "Bank User", "Address": "Baku", "Phone": "+994700000000", "Gender": "Male", "Position": "iOS Developer", "Birthdate": "1900-01-01" , "Email": "office@accessbank.az"]
        
        
        api.Request(shortUrl: "\(customer)", params: param, method: .post, headers: nil) { (snapshot, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                return
            }
                        
            self.fetchAllCustomer { (customers) in
                completion(customers)
            }
            
        }
    }
    
    
    
    func deleteSelectedRow(id: Int, completion: @escaping() -> ()) {
        
        let param =  ["Id": id]
        request("\(baseURL)/\(customer)", method: .delete, parameters: param, encoding: JSONEncoding.default).responseString { (response) in
            completion()
        }
    }
    
    
    
    func updateSelecedUser(id: Int, name: String, address: String, phone: String, gender: String, position: String, dob: String, email: String, completion: @escaping() -> ()) {
        
        
        let formatDate = api.DateFormatForString(dateStr: dob, dateFormat: "dd.MM.yyyy", format: "yyyy-MM-dd")
        
        let param = ["Id": "\(id)", "Name": "\(name)", "Address": "\(address)", "Phone": "\(phone)", "Gender": "\(gender)", "Position": "\(position)", "Birthdate": "\(formatDate ?? "2000-01-01")" , "Email": "\(email)"]
                
        api.Request(shortUrl: "\(customer)", params: param, method: .put, headers: nil) { (snapshot, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            completion()
            
        }
        
    }
    
    
    
    
    
    
    func FetchCustomerData(comletion: @escaping(Customer)) {

        api.RequestAPI(model: CustomerModel.self, responseType: .array, urlType: "") { (customer, _) in
            if let cust = customer?.data {
                comletion(cust)
            }
        }
        
    }
}




