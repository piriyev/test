//
//  UserCell.swift
//  TessAccessBank
//
//  Created by Habiboff on 12.07.20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    
    var userData: UserDataModel? {
        didSet {
            
            if let title = userData?.title {
                self.addressLabel.text = title
            }
            
            if let value = userData?.value {
                self.addressValueLabel.text = value
            }
            
        }
    }
    
    let addressLabel: UILabel = {
          let label = UILabel()
          label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
          label.textAlignment = .left
          return label
      }()

      
      let addressValueLabel: UILabel = {
          let label = UILabel()
          label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
          label.textAlignment = .left
          return label
      }()
    
    
    func setupView() {
        
        
        addressLabel.anchorView(view: self, topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 6, leftConstant: 15, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 20)
        addressValueLabel.anchorView(view: self, addressLabel.bottomAnchor, left: addressLabel.leadingAnchor, bottom: nil, right: addressLabel.trailingAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        
    }
    
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
