//
//  Results.swift
//  TessAccessBank
//
//  Created by AccessBank on 7/24/20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import Foundation


struct ResultArray<T: Decodable>: Decodable {
    let success: Bool
    let responseCode: Int?
    let data: [T]?
    
}


struct ResulstObject<T: Decodable>: Decodable {
    let success: Bool
    let responseCode: Int?
    let data: T?
}
