//
//  UserDataModel.swift
//  TessAccessBank
//
//  Created by Habiboff on 12.07.20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import Foundation


struct UserDataModel: Decodable {
    let title: String
    let value: String
}
