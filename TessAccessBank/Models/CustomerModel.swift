//
//  CustomerModel.swift
//  TessAccessBank
//
//  Created by Nahid Habibov on 7/12/20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import Foundation



struct CustomerModel: Decodable {
    let Id: Int
    let Name: String?
    let Address: String?
    let Birthdate: String?
    let Email: String?
    let Gender: String?
    let Phone: String?
    let Position: String?
}
