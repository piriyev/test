//
//  IndentedTextField.swift
//  LBTATools
//
//  Created by Brian Voong on 5/7/19.
//

import UIKit

open class IndentedTextField: UITextField {
    
    let padding: CGFloat
    
    public init(placeholder: String? = nil, alignment: NSTextAlignment = .left, text: String = "", font: UIFont = UIFont.systemFont(ofSize: 16, weight: .regular), textColor: UIColor = .black, padding: CGFloat = 0, cornerRadius: CGFloat = 0, keyboardType: UIKeyboardType = .default, backgroundColor: UIColor = .clear, isSecureTextEntry: Bool = false, bottomLine: Bool = false) {
        self.padding = padding
        super.init(frame: .zero)
        self.placeholder = placeholder
        layer.cornerRadius = cornerRadius
        self.backgroundColor = backgroundColor
        self.keyboardType = keyboardType        
        self.isSecureTextEntry = isSecureTextEntry
        self.text = text
        self.textAlignment = alignment
        self.textColor = textColor
        self.font = font
        self.doneAccessory = true
        
        if bottomLine {
            self.LineStyle()
        }
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
