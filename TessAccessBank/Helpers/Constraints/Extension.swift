//
//  Extension.swift
//  Extension
//
//  Created by Habiboff's
//
//

import UIKit
import Foundation
import SystemConfiguration
import LocalAuthentication
//import Alamofire


//extension BarManagers {
//
//    func hideTabBarView() {
//
//        self.contentConstraints?[1].constant = 110
//        UIView.animate(withDuration: 0.5, animations: {
//            self.keyWindow?.layoutIfNeeded()
//        }, completion: nil)
//    }
//
//}
 

extension UIViewController {
    
    func NotificationCreateFunc(rawValue: String, action: String) {
        
        NotificationCenter.default.addObserver(self, selector: Selector(action), name:NSNotification.Name(rawValue: rawValue), object: nil)
    }
    
    
    func NotifiyPostFunc(rawValue: String) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: rawValue), object: nil)
    }
    
    
 
    
}



extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}


extension UserDefaults {
    
    static func exists(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
}



public extension UIView {
    
    func shake(count : Float = 4,for duration : TimeInterval = 0.5,withTranslation translation : Float = 5) -> Bool {
        
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.repeatCount = count
        animation.duration = duration/TimeInterval(animation.repeatCount)
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: CGFloat(-translation), y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: CGFloat(translation), y: self.center.y))
        layer.add(animation, forKey: "shake")
        return true
    }
}



extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}



extension UIViewController {

    func RemoveIndicator(balckView: UIView) {
       balckView.removeFromSuperview()
    }
    
    
    
    func Localizer(key: String) -> String{
        
        return NSLocalizedString(key, comment: "Data")
        
    }
    
   
    func NotInternet() {
        
        self.AlertMessage(titles: Localizer(key: "alertTitle"), messages: Localizer(key: "nointernetconnect"), actiontitle: Localizer(key: "buttontitle"))
    }
    
    
    func DateFormatForString(dateStr: String?, format: String) -> String? {
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        if let dateString = dateStr {
            if let date = dateFormatter.date(from: dateString) {
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = format
                return dateFormat.string(from: date)
            }
        }
        
        return nil
    }
    
    
    func DateFormatForString(dateStr: String?, dateFormat: String, format: String) -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        if let dateString = dateStr {
            if let date = dateFormatter.date(from: dateString) {
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = format
                return dateFormat.string(from: date)
            }
        }
        
        return nil
    }
    
    
}




extension UITableViewCell {
    
    func DateFormatForString(dateStr: String?, format: String) -> String? {
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        if let dateString = dateStr {
            if let date = dateFormatter.date(from: dateString) {
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = format
                return dateFormat.string(from: date)
            }
        }
        
        return nil
    }
}


extension UICollectionViewCell {
    
    func DateFormatForString(dateStr: String?, format: String) -> String? {
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        if let dateString = dateStr {
            if let date = dateFormatter.date(from: dateString) {
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = format
                return dateFormat.string(from: date)
            }
        }
        
        return nil
    }

    func DateFormatForStringAZ(dateStr: String?, format: String) -> String? {
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        if let dateString = dateStr {
            if let date = dateFormatter.date(from: dateString) {
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = format
                return dateFormat.string(from: date)
            }
        }
        
        return nil
    }
}


extension UIViewController {
    
    
    func createUserDefaults(value: Bool, key: String) {
        let userDef = UserDefaults.standard
        userDef.set(value, forKey: key)
        userDef.synchronize()

    }
    
    
    func removeKeyUserDefault(key: String) {
        let userDef = UserDefaults.standard
        userDef.removeObject(forKey: key)
//        userDef.synchronize()
    }
    
    
    func AuthControlFaceID(completion: @escaping(Bool) -> ()) {
        let authenticationContext = LAContext()
        var error:NSError?
        
        let isValidSensor : Bool = authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
        
        if isValidSensor {
            completion(true)
        } else {
            completion(false)
        }
        
    }
    
    
    
    func Authenticate(completion: @escaping ((_ response: Bool) -> ()) ) {
        
        let authenticationContext = LAContext()
         
        var error:NSError?
        
        
       
        let isValidSensor : Bool = authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)

        if isValidSensor {
        
            
            authenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
            localizedReason: "Touch / Face ID authentication",
            reply: { [unowned self] (success, error) -> Void in
               
               if(success) {
                
                completion(true)
                
               } else {
                 completion(false)
                
                
                   if let error = error {
                       let _ = self.errorMessage(errorCode: error._code)
                   }
                   
                }
             })
       
        } else {
           
            let strMessage = self.errorMessage(errorCode: (error?._code)!)
            if strMessage != "" {
                //self.showAlertWithTitle(title: "Error", message: strMessage)
            }
        }
        
    }
    
    

    func errorMessage(errorCode:Int) -> String{
        
        var strMessage = ""
        
        switch errorCode {
            
        case LAError.Code.authenticationFailed.rawValue:
            strMessage = "Authentication Failed"
            
        case LAError.Code.appCancel.rawValue:
            strMessage = "User Cancel"
            
        case LAError.Code.systemCancel.rawValue:
            strMessage = "System Cancel"
            
        case LAError.Code.passcodeNotSet.rawValue:
            strMessage = "Please goto the Settings & Turn On Passcode"
            
//        case LAError.Code.touchIDNotAvailable.rawValue:
//            strMessage = "TouchI or FaceID DNot Available"
//
//        case LAError.Code.touchIDNotEnrolled.rawValue:
//            strMessage = "TouchID or FaceID Not Enrolled"
//
//        case LAError.Code.touchIDLockout.rawValue:
//            strMessage = "TouchID or FaceID Lockout Please goto the Settings & Turn On Passcode"
            
        case LAError.Code.appCancel.rawValue:
            strMessage = "App Cancel"
            
        case LAError.Code.invalidContext.rawValue:
            strMessage = "Invalid Context"
            
        default:
            strMessage = ""
            
        }
        return strMessage
    }
    
    
    
    
    func showAlertWithTitle( title:String, message:String ) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let actionOk = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(actionOk)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
}

 
    
extension UIViewController {
    
    
    func setBackgroundStyle(imagename: String, view: UIView) {
  
        var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        
        
        
        navigationController?.navigationBar.isTranslucent = false
//        UIApplication.shared.statusBarStyle = .lightContent
        UIGraphicsBeginImageContext(view.frame.size)
        UIImage(named: imagename)?.draw(in: view.bounds)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        view.backgroundColor = UIColor(patternImage: image)
        
    }

    
    
}






extension UIViewController {
    
    
    enum PresentMdoe {
        case fullScreen
        case none
    }
    
    
    func showControllerPresent(controller: UIViewController, presenMode: PresentMdoe) {
        
        switch presenMode {
        case .fullScreen:
            controller.modalPresentationStyle = .fullScreen
        case .none:
            controller.modalPresentationStyle = .none
        }
        
        self.present(controller, animated: true, completion: nil)
        
    }
    
    
    
    func showControllerPush(controller: UIViewController, presenMode: PresentMdoe) {
        
        switch presenMode {
        case .fullScreen:
            controller.modalPresentationStyle = .fullScreen
        case .none:
            controller.modalPresentationStyle = .none
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    
    func navigationControllerStyle(titles: String, Colors: UIColor, fontsize: CGFloat, fontColors: UIColor) {
 
        
        navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = titles
        navigationController?.navigationBar.barTintColor = Colors
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let fonAndSize = UIFont.boldSystemFont(ofSize: 18) //UIFont(name: "Helvetica", size: fontsize)!
        navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: fonAndSize, NSAttributedString.Key.foregroundColor: fontColors]
        navigationController?.navigationBar.tintColor = fontColors
        
        
    }
    
    
    func Reachability() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    
    
}




extension AppDelegate {
    
    func Reachability() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
}

extension UIApplication {
    var statusBarUIView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 384824
            if let statusBar = self.keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBarView.tag = tag

                self.keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else {
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }
}


extension UIApplication {
    
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}






extension UIViewController {
    
  
    
    func rightBarbutton(selectors: String, image: String) {
        
        let barButton = UIBarButtonItem(image: UIImage(named: image), style: .plain, target: self, action: Selector(selectors))
        navigationItem.rightBarButtonItem = barButton
    }
    

    func leftBarbutton(selectors: String, image: String) {
        
        let barButton = UIBarButtonItem(image: UIImage(named: image), style: .plain, target: self, action: Selector(selectors))
        navigationItem.leftBarButtonItem = barButton
    }
    
    
    
    func searchBarbutton(selectors: String) {
        
        let barButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: Selector(selectors))
        
        navigationItem.rightBarButtonItem = barButton
    }
    
    
    
    func AlertMessage(titles: String, messages: String, actiontitle: String) {
        
        let alert = UIAlertController(title: titles, message: messages, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actiontitle, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func AlertMessageFunc(titiles: String, messages: String, actiontitle: String, handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: titiles, message: messages, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actiontitle, style: .default, handler: handler))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func AlertMessageQuest(titles: String, messages: String, noTitle: String, yesTitle: String, canceTitle: String = "Then", completion: @escaping(_ yesHandler: UIAlertAction?, _ noHandler: UIAlertAction?) -> ()) {
        
        let alert = UIAlertController(title: titles, message: messages, preferredStyle: .alert)

        let acceptAction = UIAlertAction(title: yesTitle, style: .default) { (yesAction) in
            completion(yesAction, nil)
        }
        
        let diclineAction = UIAlertAction(title: noTitle, style: .default) { (noAction) in
            completion(nil, noAction)
        }
        
       // let cancel = UIAlertAction(title: canceTitle, style: .cancel, handler: nil)

        alert.addAction(diclineAction)
        alert.addAction(acceptAction)
        //alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func AlertQuest(titles: String, messages: String, noTitle: String, yesTitle: String, completion: @escaping(UIAlertAction) -> ()) {
        
        let alert = UIAlertController(title: titles, message: messages, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: yesTitle, style: .default, handler: completion)
        let diclineAction = UIAlertAction(title: noTitle, style: .default, handler: nil)
        
        alert.addAction(diclineAction)
        alert.addAction(acceptAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    
    func FormatTextField(number: String, mask: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = mask //"XXX - XX - XX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
                
            } else {
                result.append(ch)
            }
        }    
        return result
    }
}



extension Date {
    
    func getDaysInMonth() -> Int {
        let calendar = Calendar.current
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        return numDays
    }
    
    
}



extension UITextView {
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            
            if hasDone {
                addDoneButtonOnKeyboard(title: "Done")
            }
        }
    }

    func addDoneButtonOnKeyboard(title: String)  {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: title, style: .done, target: self, action: #selector(doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }

    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
}




extension UIButton {

    func centerVertically(padding: CGFloat = 6.0) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
            return
        }

        let totalHeight = imageViewSize.height + titleLabelSize.height + padding

        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageViewSize.height),
            left: 0.0,
            bottom: 0.0,
            right: -titleLabelSize.width
        )

        self.titleEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: -imageViewSize.width,
            bottom: -(totalHeight - titleLabelSize.height),
            right: 0.0
        )

        self.contentEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: titleLabelSize.height,
            right: 0.0
        )
    }

}



extension UITextField {
   
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            
            if hasDone {
                addDoneButtonOnKeyboard(title: "Done")
            }
        }
    }

    func addDoneButtonOnKeyboard(title: String)  {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: title, style: .done, target: self, action: #selector(doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }

    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
    
    
    
    func LineStyle(color: UIColor = UIColor(white: 0.80, alpha: 1), lineHeight: CGFloat = 1) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.anchorView(view: self, nil, left: self.leadingAnchor, bottom: self.bottomAnchor, right: self.trailingAnchor, topConstant: 0, leftConstant: -5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: lineHeight)
    }
    
    
    func LeftViewImage(image: String) {
        let imageView = UIImageView(image: UIImage(named: image))
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 0, y: 10, width: 20, height: 20)
        let view = UIView()
        view.backgroundColor = .clear
        view.addSubview(imageView)
        view.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        self.leftViewMode = .always
        self.leftView = view
    }
    
    
    func RightViewImage(image: UIImage, imageW: CGFloat = 20, imageH: CGFloat = 20) {
        let view = UIView()
        view.backgroundColor = .clear
        view.frame = CGRect(x: 0, y: 0, width: 35, height: self.frame.height)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.center = view.center
        view.addSubview(imageView)
        self.rightViewMode = .always
        self.leftViewMode  = .always
        self.rightView = view
    }
    
    
    
    func RoundComboActiveIconRight(image: UIImage, gesture: UIGestureRecognizer) {
        
        self.borderStyle = .none
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.anchorView(view: self, nil, left: nil, bottom: nil, right: self.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 3, widthConstant: 12, heightConstant: 12)
        imageView.anchorCenterYToSuperview(constant: 2)
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(gesture)
    }
    
    
    
    
    func RoundComboActiveIconLeft(image: String) {
        
        self.borderStyle = .none
        let imageView = UIImageView(image: UIImage(named: image))
        imageView.contentMode = .scaleAspectFit
        imageView.anchorView(view: self, nil, left: self.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 10, heightConstant: 10)
        imageView.anchorCenterYToSuperview()
    }
    
}



extension UIView {

    func addLineViewController(anchorTop: NSLayoutYAxisAnchor, top: CGFloat, padding: CGFloat, left: CGFloat = 20, right: CGFloat = 20) {
        
        let lineView = UIView(backgroundColor: #colorLiteral(red: 0.9098039216, green: 0.9137254902, blue: 0.9215686275, alpha: 1), radius: 0)
        lineView.anchorView(view: self, anchorTop, left: self.leadingAnchor, bottom: nil, right: self.trailingAnchor, topConstant: 20, leftConstant: left, bottomConstant: 0, rightConstant: right, widthConstant: 0, heightConstant: 1)
    }
    
    
    func  setShadowElement(cornerRadius: CGFloat = 0, shadowColor: UIColor, shadowOffset: CGSize, shadowRadius: CGFloat, shadowOpacity: Float) {
        
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = shadowOpacity
    }
    
}



extension UIButton {
        
    func showImageLabel(image: UIImage, text: String, titleSiz: CGFloat, imageSize: CGFloat) {
    
        self.layer.cornerRadius = 10
        self.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.08181330472).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        self.layer.shadowRadius = 20
        self.layer.shadowOpacity = 1
        self.imageView?.contentMode = .scaleAspectFit
        self.setImage(image, for: .normal)
        self.setTitle(text, for: .normal)
        self.setTitleColor(.black, for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        self.titleEdgeInsets = UIEdgeInsets(top: titleSiz, left: 0, bottom: 0, right: 40)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: imageSize, bottom: 10, right: 0)
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
         
    }
    
}









extension UIFont {
    
    enum Fonts {
        
        case black
        case bold
        case medium
        case regular
        case light
        case thin
    }
    
    
 
//    convenience init(f: Fonts, size: CGFloat) {
//        self.init()
//        UILabel.appearance().font = customFont(fonts: f, size: size)
//    }
    
    
    
     func customFont(fonts: Fonts, size: CGFloat) -> UIFont! {
        
        switch fonts {
            case .black:
                return UIFont(name: "Roboto-Black", size: size)
            case .bold:
                return UIFont(name: "Roboto-Bold", size: size)
            case .medium:
                return UIFont(name: "Roboto-Medium", size: size)
            case .regular:
                return UIFont(name: "Roboto-Regular", size: size)
            case .light:
                return UIFont(name: "Roboto-Light", size: size)
            case .thin:
                return UIFont(name: "Roboto-Thin", size: size)
        }
    }
    
    
}



extension UILabel {
    
    
    var substituteFontNameLight : String {
        get { return self.font.fontName }
        set {
            if self.font.fontName.range(of:"Light") == nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }

    var substituteFontNameRegular : String {
        get { return self.font.fontName }
        set {
            if self.font.fontName.range(of:"Regular") != nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }

    var substituteFontNameMedium : String {
        get { return self.font.fontName }
        set {
            if self.font.fontName.range(of:"Medium") == nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }
    
    
    var substituteFontNameBold : String {
        get { return self.font.fontName }
        set {
            if self.font.fontName.range(of:"Bold") == nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }

   
    
    var substituteFontNameBlack : String {
        get { return self.font.fontName }
        set {
            if self.font.fontName.range(of:"Black") == nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }
    
    
    var substituteFontNameThin : String {
        get { return self.font.fontName }
        set {
            if self.font.fontName.range(of:"Thin") == nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }
    

    var substituteFontName : String {
            get { return self.font.fontName }
            set { self.font = UIFont(name: newValue, size: self.font.pointSize) }
    }

    
}




// Alamofire



extension UILabel {
    
    enum CharType {
        case normal, bold
    }
    
    func attributedLabel(boldCharacter: CharType) {
        
        switch boldCharacter {
        case .normal:
            self.attributedText = attributedText(withString: String(self.text!), font: self.font)
        case .bold :
            self.attributedText = attributedText(withString: String(self.text!), boldString: self.text!, font: self.font)
        }
        
        
//        if boldCharacter == "N" {
//            self.attributedText = attributedText(withString: String(self.text!), boldString: "", font: self.font)
//        } else if boldCharacter == "B" {
//            self.attributedText = attributedText(withString: String(self.text!), boldString: self.text!, font: self.font)
//        }
    }
    
    
    func attributedText(withString string: String, boldString: String = "", font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
    
    
}



extension UITextField {
    
     func PhoneFormatTextField(number: String, mask: String) {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = mask
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        self.text = result
    }
    
}



extension UIApplication {

    static func rootViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return rootViewController(base: nav.visibleViewController)
        }

        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return rootViewController(base: selected)
        }

        if let presented = base?.presentedViewController {
            return rootViewController(base: presented)
        }

        return base
    }
}



extension UINavigationController {
    open override var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }

    open override var childForStatusBarHidden: UIViewController? {
        return self.topViewController
    }
}
