//
//  UIButton.swift
//  LBTATools
//
//  Created by Brian Voong on 5/1/19.
//

import UIKit

extension UIButton {
    
    convenience public init(title: String, titleColor: UIColor, tag: Int = 0, font: UIFont = .systemFont(ofSize: 14), backgroundColor: UIColor = .clear,  cornerRadius: CGFloat = 0, target: Any? = nil, action: Selector? = nil) {
        self.init(type: .system)
        setTitle(title, for: .normal)
        setTitleColor(titleColor, for: .normal)
        self.titleLabel?.font = font
        self.layer.cornerRadius = cornerRadius
        self.backgroundColor = backgroundColor
        if let action = action {
            addTarget(target, action: action, for: .touchUpInside)
        }
        self.clipsToBounds = true
        self.tag = tag
    }
    
    convenience public init(image: UIImage, title: String? = nil, titleColor: UIColor = .black, contentMode: UIControl.ContentMode =  .scaleAspectFill, font: UIFont = .systemFont(ofSize: 10), tintColor: UIColor? = nil, backgroundColor: UIColor = .clear, cornerRadius: CGFloat = 0, target: Any? = nil, action: Selector? = nil) {
        self.init(type: .system)
      
        if tintColor == nil {
            setImage(image, for: .normal)
        } else {
            setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
            self.tintColor = tintColor
            imageView?.contentMode = contentMode
        }
        
        setTitle(title, for: .normal)
        setTitleColor(titleColor, for: .normal)
        self.titleLabel?.font = font
        self.layer.cornerRadius = cornerRadius
        self.backgroundColor = backgroundColor
        
        if let action = action {
            addTarget(target, action: action, for: .touchUpInside)
        }
        self.clipsToBounds = true
    }
    
    

    
    
    
    
}
