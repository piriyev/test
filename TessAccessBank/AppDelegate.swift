//
//  AppDelegate.swift
//  TessAccessBank
//
//  Created by Nahid Habibov on 7/11/20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        
        window?.rootViewController = UINavigationController(rootViewController: HomeViewController())
        
        return true
    }

    // MARK: UISceneSession Lifecycle




}

