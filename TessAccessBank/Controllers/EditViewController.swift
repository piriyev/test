//
//  EditViewController.swift
//  TessAccessBank
//
//  Created by Nahid Habibov on 7/12/20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import UIKit


class EditViewController: UIViewController {
    
    var userData: CustomerModel? {
        
        didSet {
            
            if let name = userData?.Name {
                self.nameTextField.text = name
            }
            
            
            if let address = userData?.Address {
                self.addressTextField.text = address
            }
            
            
            if let gender = userData?.Gender {
                self.genderTextField.text = gender
            }
            
            
            if let position = userData?.Position {
                self.positionTextField.text = position
            }
            
            
            if let dob = userData?.Birthdate {
                self.dobTextField.text = DateFormatForString(dateStr: dob, dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", format: "dd.MM.yyyy")
            }
            
            
            self.phoneTextField.text = userData?.Phone ?? "N/A"
            
            
            if let mail = userData?.Email {
                self.emailTextField.text = mail
            }
            
        }
    }
    
    
    
    let nameLabel: UILabel = {
       let label = UILabel()
        label.text = "Full Name:"
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        label.textAlignment = .left
        return label
    }()
    
    let nameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Full Name"
        textField.font = .systemFont(ofSize: 16, weight: .regular)
        textField.borderStyle = .roundedRect
        return textField
    }()

    let addressLabel: UILabel = {
       let label = UILabel()
        label.text = "Address:"
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        label.textAlignment = .left
        return label
    }()


    let addressTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Address"
        textField.font = .systemFont(ofSize: 16, weight: .regular)
        textField.borderStyle = .roundedRect
        return textField
    }()

    
    let genderLabel: UILabel = {
       let label = UILabel()
        label.text = "Gender:"
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        label.textAlignment = .left
        return label
    }()

    
    let genderTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Gender"
        textField.font = .systemFont(ofSize: 16, weight: .regular)
        textField.borderStyle = .roundedRect
        return textField
    }()

    
    let positionLabel: UILabel = {
       let label = UILabel()
        label.text = "Position"
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        label.textAlignment = .left
        return label
    }()

    
    
    let positionTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Position"
        textField.font = .systemFont(ofSize: 16, weight: .regular)
        textField.borderStyle = .roundedRect
        return textField
    }()

    
    let dobLabel: UILabel = {
       let label = UILabel()
        label.text = "Date of birth:"
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        label.textAlignment = .left
        return label
    }()

    
    
    let dobTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "dd.MM.yyyy"
        textField.font = .systemFont(ofSize: 16, weight: .regular)
        textField.borderStyle = .roundedRect
        return textField
    }()
    
    
    let phoneLabel: UILabel = {
       let label = UILabel()
        label.text = "Phone:"
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        label.textAlignment = .left
        return label
    }()


    let phoneTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Phone"
        textField.font = .systemFont(ofSize: 16, weight: .regular)
        textField.borderStyle = .roundedRect
        return textField
    }()
    

    let emailLabel: UILabel = {
       let label = UILabel()
        label.text = "Email:"
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        label.textAlignment = .left
        return label
    }()

    
    let emailTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Email"
        textField.font = .systemFont(ofSize: 16, weight: .regular)
        textField.borderStyle = .roundedRect
        return textField
    }()

    
    
    let scrollView: UIScrollView = {
       let view = UIScrollView()
        view.delaysContentTouches = false
        return view
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Edit Information"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(handleDismiss))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .done, target: self, action: #selector(handleEditInfo))
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        self.showComponents()
    }
    
    
    fileprivate func showComponents() {
        
        
        nameLabel.anchorView(view: self.view, view.safeAreaLayoutGuide.topAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        
        nameTextField.anchorView(view: self.view, nameLabel.bottomAnchor, left: nameLabel.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 35)
        
        
        addressLabel.anchorView(view: self.view, nameTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        
        addressTextField.anchorView(view: self.view, addressLabel.bottomAnchor, left: nameLabel.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 35)


        genderLabel.anchorView(view: self.view, addressTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        
        genderTextField.anchorView(view: self.view, genderLabel.bottomAnchor, left: nameLabel.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 35)

        
        
        positionLabel.anchorView(view: self.view, genderTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        
        positionTextField.anchorView(view: self.view, positionLabel.bottomAnchor, left: nameLabel.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 35)

        
        
        dobLabel.anchorView(view: self.view, positionTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        
        dobTextField.anchorView(view: self.view, dobLabel.bottomAnchor, left: nameLabel.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 35)

        
        
        phoneLabel.anchorView(view: self.view, dobTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        
        phoneTextField.anchorView(view: self.view, phoneLabel.bottomAnchor, left: nameLabel.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 35)

        
        emailLabel.anchorView(view: self.view, phoneTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 20, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        
        emailTextField.anchorView(view: self.view, emailLabel.bottomAnchor, left: nameLabel.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 35)


        
        
    }
 
    
    
    @objc func handleEditInfo() {
        
        self.view.endEditing(true)
        if let id = userData?.Id {
            
            Services.updateSelecedUser(id: id, name: nameTextField.text ?? "N/A", address: addressTextField.text ?? "N/A", phone: phoneTextField.text ?? "N/A", gender: genderTextField.text ?? "N/A", position: positionTextField.text ?? "N/A", dob: dobTextField.text ?? "N/A", email: emailTextField.text ?? "N/A") {
                
                    self.AlertMessageFunc(titiles: "Success", messages: "Has been changed successfully", actiontitle: "OK") { (func) in
                        self.dismiss(animated: true, completion: nil)
                    }
            }
        }
    }
    
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    
    @objc func handleDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
