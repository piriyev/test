//
//  DetailViewController.swift
//  TessAccessBank
//
//  Created by Nahid Habibov on 7/11/20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import UIKit


// Override
// Protocols
// Fileprivate vs Private
// lazy
// Decodable, Codable, Hasable
// Completion Handler



class DetailViewController: UIViewController {
    
    var delegate: HomeViewController?
    var userData: CustomerModel? {
        didSet {
            
            if let id = userData?.Id {
                self.userId = id
            }
            
            
            if let name = userData?.Name {
                self.profileNameLabel.text = name
            }
            
            
            if let address = userData?.Address {
                self.InformationLabel.text = address
            }
            
        }
    }
    
    
    let containerView = UIView(backgroundColor: #colorLiteral(red: 0.9241450259, green: 0.9241450259, blue: 0.9241450259, alpha: 1), radius: 0)
    let informationView = UIView(backgroundColor: .white, radius: 6)
    
    var userId: Int?
    
    let profileImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "preview_image")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    
    let profileNameLabel: UILabel = {
       let label = UILabel()
        label.font = .systemFont(ofSize: 18, weight: .medium)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        label.textAlignment = .left
        return label
    }()
    
    let InformationLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15, weight: .regular)
        label.textColor = #colorLiteral(red: 0.08647344323, green: 0.09306016633, blue: 0.1022765344, alpha: 1)
        return label
    }()

    
    
    lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        return button
    }()
    
    
    lazy var editInformationButton: UIButton = {
        let button = UIButton(type: .system)
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(#imageLiteral(resourceName: "edit").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(changeInformation), for: .touchUpInside)
        return button
    }()
    
    
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = .white
        view.separatorStyle = .none
        view.bounces = false
        return view
    }()
    
    private let cellId = "cellId"
    var userDataModel = [UserDataModel]()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.showComponents()
    }
    
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegate?.userNameData.removeAll()
        
        Services.fetchAllCustomer { (customers) in
            if let customer = customers {
                self.delegate?.userNameData = customer
                self.delegate?.tableView.reloadData()
            }
        }
        
        
    }
    
    
    fileprivate func showComponents() {
        
        self.userDetailInformation()
        profileImageView.anchorView(view: self.view, view.topAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 260)
        
        backButton.anchorView(view: self.view, view.safeAreaLayoutGuide.topAnchor, left: view.leadingAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 24, heightConstant: 24)
        
        
        containerView.anchorView(view: self.view, profileImageView.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 60)
        
        
        profileNameLabel.anchorView(view: self.containerView, containerView.topAnchor, left: containerView.leadingAnchor, bottom: nil, right: containerView.trailingAnchor, topConstant: 10, leftConstant: 20, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 20)
        
        InformationLabel.anchorView(view: self.containerView, profileNameLabel.bottomAnchor, left: profileNameLabel.leadingAnchor, bottom: nil, right: nil, topConstant: 2, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 18)
        

        editInformationButton.anchorView(view: self.containerView, nil, left: nil, bottom: nil, right: containerView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 27, heightConstant: 27)
        editInformationButton.anchorCenterYToSuperview()
        
        informationView.setupShadow(opacity: 0.2, radius: 6.0, offset: CGSize(width: 0.0, height: 0.0), color: #colorLiteral(red: 0.2745098039, green: 0.2745098039, blue: 0.2745098039, alpha: 0.6862081684))
        informationView.anchorView(view: self.view, containerView.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 20, rightConstant: 15, widthConstant: 0, heightConstant: 360.0)
        
        
        
        
        tableView.anchorView(view: self.informationView, informationView.topAnchor, left: informationView.leadingAnchor, bottom: informationView.bottomAnchor, right: informationView.trailingAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        

        
    }
    
    
    fileprivate func userDetailInformation() {
        
        if let id = userId {
            Services.fetchUserById(id: id) { (customers) in
                
                if let customer = customers {
                    
                    let date = self.DateFormatForString(dateStr: customer.Birthdate, dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", format: "dd.MM.yyyy")
                    self.userDataModel = [
                        
                        UserDataModel(title: "Gender", value: customer.Gender ?? "N/A"),
                        UserDataModel(title: "Position:", value: customer.Position ?? "N/A"),
                        UserDataModel(title: "Date of birth:", value: date ?? "N/A"),
                        UserDataModel(title: "Phone:", value: customer.Phone ?? "N/A"),
                        UserDataModel(title: "E-mail:", value: customer.Email ?? "N/A")
                        
                    ]
                    
                    self.tableView.reloadData()
                    self.userData = customer
                }
            }
        }
    }
    
    
    
    
    @objc func changeInformation() {
        
        let editView = EditViewController()
        editView.userData = userData
        let nc = UINavigationController(rootViewController: editView)
        present(nc, animated: true, completion: nil)
        
    }
    
    
    @objc func handleBack() {
        navigationController?.popViewController(animated: true)
    }
    
    
}


extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userDataModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        cell.selectionStyle = .none
        
        
        if (indexPath.row % 2 == 1) {
            cell.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.addressLabel.textColor = .white
            cell.addressValueLabel.textColor = .white
        } else {
            cell.backgroundColor = .white
        }
        
        
        
        cell.userData = userDataModel[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    
}

