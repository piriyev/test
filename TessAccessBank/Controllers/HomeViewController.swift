//
//  ViewController.swift
//  TessAccessBank
//
//  Created by Nahid Habibov on 7/11/20.
//  Copyright © 2020 Nahid Habibov. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    var userNameData = [CustomerModel]()
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = .white
        return view
    }()
    
    private let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        title = "Test AccessBank"
        view.backgroundColor = .white
        self.showComponents()
            
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New Item", style: .done, target: self, action: #selector(handleAddNewItem))
        
    
        
    }
 

     
    fileprivate func showComponents() {
  
//        Services.FetchCustomerData { (customer) in
//            self.userNameData = customer
//            self.tableView.reloadData()
//        }
        
        
        Services.fetchAllCustomer { (customerData) in

            if let customer = customerData {
                self.userNameData = customer
                self.tableView.reloadData()
            }
        }
        
        tableView.anchorView(view: self.view, view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    
    func showDetail(index: Int) {
        
        let detail = DetailViewController()
        detail.delegate = self
        detail.userData = userNameData[index]
        navigationController?.show(detail, sender: nil)
    }
 
    
    @objc func handleAddNewItem() {
        Services.addUserPost { (customers) in
            if let customer = customers {
                self.userNameData.removeAll()
                self.userNameData = customer
                self.tableView.reloadData()
            }
        }
    }
     
    
}



extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userNameData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)        
        cell.textLabel?.text = userNameData[indexPath.row].Name
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = userNameData[indexPath.row].Id
        print("ID", id)
        self.showDetail(index: indexPath.row)
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            let id = userNameData[indexPath.row].Id
            Services.deleteSelectedRow(id: id) {
                self.tableView.beginUpdates()
                self.userNameData.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
                self.tableView.endUpdates()
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}
